﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarchingCubesMain : MonoBehaviour {

    public Material marchingMaterial;
    private MarchingCubes marching;

    List<GameObject> generatedMeshes = new List<GameObject>();

    private float isoLevel = 3;
    [SerializeField]
    private Slider isoSlider = null;
    [SerializeField]
    private AlgorithmSetup setup = null;

    private bool wasLastMeshRandom;

    private int randomDimension;

    //handle all hotkeys
    private void Update() {
        if (Input.GetKeyDown(KeyCode.M)) {
            Debug.Log("Marching cubes...");
            wasLastMeshRandom = false;
            CalculateValues();
        }
        if (Input.GetKeyDown(KeyCode.N)) {
            Debug.Log("Marching cubes random...");
            randomDimension = 100;
            wasLastMeshRandom = true;
            GameManager.instance.CleanUp();
            setup.GenerateRandom(randomDimension);
            CalculateValues(true);
        }
        if (Input.GetKeyDown(KeyCode.B)) {
            Debug.Log("Marching cubes random...");
            randomDimension = 200;
            wasLastMeshRandom = true;
            GameManager.instance.CleanUp();
            setup.GenerateRandom(randomDimension);
            CalculateValues(true);
        }
        if (Input.GetKey(KeyCode.UpArrow)) {
            isoLevel = Mathf.Min(7f, isoLevel + 0.1f);
            isoSlider.value = isoLevel;
            GenerateMesh(wasLastMeshRandom);
        }
        if (Input.GetKey(KeyCode.DownArrow)) {
            isoLevel = Mathf.Max(1f, isoLevel - 0.1f);
            isoSlider.value = isoLevel;
            GenerateMesh(wasLastMeshRandom);
        }
        if (Input.GetKeyDown(KeyCode.Return)) {
            GameManager.instance.ToggleVoxelVisibility();
        }
    }

    private void CalculateValues(bool random = false) {
        if (random) {
            GenerateMesh(true);
        } else {
            setup.CalculateValues(GameManager.instance.GetVoxels().Keys);
            GenerateMesh();
        }
        
        
    }

    public void GenerateMesh(bool random = false) {
        //remove old meshes
        foreach (GameObject mesh in generatedMeshes) {
            Destroy(mesh);
        }
        generatedMeshes = new List<GameObject>();

        marching = new MarchingCubes(isoLevel);

        //The size of voxel array.
        //add/subtract one from the bounds, since we added "dummy" cubes
        //subtract an additional one from the lower bounds, since we start adding voxel edges by incrementing the cube position by 0.5f, so need to start at -2 to get the corners at -1.5
        int lowestX = GameManager.instance.GetLowestBounds().x - 2;
        int lowestY = GameManager.instance.GetLowestBounds().y - 2;
        int lowestZ = GameManager.instance.GetLowestBounds().z - 2;
        int highestX = GameManager.instance.GetHighestBounds().x + 1;
        int highestY = GameManager.instance.GetHighestBounds().y + 1;
        int highestZ = GameManager.instance.GetHighestBounds().z + 1;
        //I'm not sure why, but for the random generation -2/+1 didn't suffice so we just take -5 and +5 to be safe without fruther investigation of the issue
        if (random) {
            lowestX = lowestY = lowestZ = -5;
            highestX = highestY = highestZ = randomDimension + 5;
        }
        int width = highestX - lowestX + 1;
        int height = highestY - lowestY + 1;
        int length = highestZ - lowestZ + 1;

        float[] voxels = new float[width * height * length];

        //retrieve all values from the ALgorithmSetup 
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < length; z++) {
                    //this is why we need to subtract 1 more from the lower bounds:
                    float fx = x + 0.5f + lowestX;
                    float fy = y + 0.5f + lowestY;
                    float fz = z + 0.5f + lowestZ;

                    int idx = x + y * width + z * width * height;
                    voxels[idx] = setup.ValueForPosition(new Vector3(fx, fy, fz));
                }
            }
        }


        List<Vector3> verts = new List<Vector3>();
        List<int> indices = new List<int>();
        marching.Generate(voxels, width, height, length, verts, indices);

        //a mesh in unity can only have up to 2^16 vertices so we split our vertices up in chunks of 30.000
        int maxVertsPerMesh = 30000; 
        int numMeshes = verts.Count / maxVertsPerMesh + 1;

        for (int i = 0; i < numMeshes; i++) {

            List<Vector3> splitVerts = new List<Vector3>();
            List<int> splitIndices = new List<int>();

            for (int j = 0; j < maxVertsPerMesh; j++) {
                int idx = i * maxVertsPerMesh + j;

                if (idx < verts.Count) {
                    splitVerts.Add(verts[idx]);
                    splitIndices.Add(j);
                }
            }

            if (splitVerts.Count == 0) {
                continue;
            }

            Mesh mesh = new Mesh();
            //provide data to the mesh
            mesh.SetVertices(splitVerts);
            mesh.SetTriangles(splitIndices, 0);
            //actually generate the mesh:
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            
            //create a gameobject and attach the mesh
            GameObject go = new GameObject("Mesh");
            go.transform.parent = transform;
            go.AddComponent<MeshFilter>();
            go.AddComponent<MeshRenderer>();
            //go.GetComponent<Renderer>().material = marchingMaterial;
            //attach the currently selected material: (alternative: dedicated marching cubes material which could have some cool poperties or shaders)
            BlockSelector selector = GameManager.instance.GetCurrentSelector();
            go.GetComponent<MeshRenderer>().material = marchingMaterial;
            go.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", selector.color);
            go.GetComponent<MeshFilter>().mesh = mesh;
            //Move to the right position:
            if (!random) {
                go.transform.localPosition = GameManager.instance.GetLowestBounds() - new Vector3(1.5f, 1.5f, 1.5f);
            }
            //and store the meshes
            generatedMeshes.Add(go);
        }
    }
}
