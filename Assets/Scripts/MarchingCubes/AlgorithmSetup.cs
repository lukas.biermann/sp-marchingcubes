﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to setup all required values for the marching cubes algorithm
public class AlgorithmSetup : MonoBehaviour {

    //contains all iso-values of the VoxelCorners by their position
    private Dictionary<Vector3, int> values;
    //contains all corner positions of each cube
    private HashSet<Vector3> vertexPositions;

    //We give the voxelPositions as a List, so we can also calculate Values for arbitrary positions without the need of gameobjects
    public void CalculateValues(IEnumerable<Vector3Int> voxelPositions) {
        values = new Dictionary<Vector3, int>();
        vertexPositions = new HashSet<Vector3>();
        //iterate through each voxel and add all VertexCoordinates without duplicates
        foreach (Vector3Int pos in voxelPositions) {
            vertexPositions.UnionWith(GetVertexCoordinatesForPosition(pos));
        }

        //start of the "neighbour count" algorithm:
        //the advantage of only using Vector3s as Keys/entities for our algorithm is that we can easily calculate and store values for our "dummy" cubes that don't really exist
        Queue<Vector3> vertexQueue = new Queue<Vector3>();

        foreach(Vector3 vertexPosition in vertexPositions) {
            //add each vertex itself
            vertexQueue.Enqueue(vertexPosition);
            foreach(Vector3 neighbourPosition in GetVertexNeighbours(vertexPosition)) {
                //and all of its neighbour positions
                vertexQueue.Enqueue(neighbourPosition);
            }
        }
        //now go through the queue and increment the neighbour count for each of them
        values = new Dictionary<Vector3, int>();
        while(vertexQueue.Count > 0) {
            Vector3 key = vertexQueue.Dequeue();
            if (values.ContainsKey(key)) {
                int oldCount = values[key];
                oldCount++;
                values[key] = oldCount;
            } else {
                values[key] = 1;
            }
        }
        Debug.Log("Calculated " + values.Count+ " values");
        //values for marching cubes are ready now!
    }
    
    //helper to get the vertex/corner coordinates of a given voxel by it's position
    private HashSet<Vector3> GetVertexCoordinatesForPosition(Vector3 pos) {
        HashSet<Vector3> coordinates = new HashSet<Vector3>();
        coordinates.UnionWith(new Vector3[] {
            pos - new Vector3(-0.5f, -0.5f, -0.5f),
            pos - new Vector3(-0.5f, -0.5f, 0.5f),
            pos - new Vector3(-0.5f, 0.5f, -0.5f),
            pos - new Vector3(-0.5f, 0.5f, 0.5f),
            pos - new Vector3(0.5f, -0.5f, -0.5f),
            pos - new Vector3(0.5f, -0.5f, 0.5f),
            pos - new Vector3(0.5f, 0.5f, -0.5f),
            pos - new Vector3(0.5f, 0.5f, 0.5f)
        });
        return coordinates;
    }

    //generate random positions inside of a given dimension via perlin noise
    //if the dimension is above 200, we increase the noise threshold, so the block islands are more spaced out and we don't generate too many blocks
    public void GenerateRandom(int dimension) {
        List<Vector3Int> generatedPositions = new List<Vector3Int>();
        float threshold = 0.5f;
        if(dimension >= 200) {
            threshold = 0.53f;
        }
        for (int x = 0; x < dimension; x++) {
            for (int y = 0; y < dimension; y++) {
                for (int z = 0; z < dimension; z++) {
                    float noiseValue = Perlin3D(x * 0.044f, y * 0.044f, z * 0.044f);//get value of the noise at given x, y, and z.
                    Vector3Int pos = new Vector3Int(x, y, z);
                    if (noiseValue >= threshold) {//is noise value above the threshold for placing a block?
                        generatedPositions.Add(pos);
                    }
                }
            }
        }
        CalculateValues(generatedPositions);
    }

    //perlin noise, same as in TerrainGenerator
    public static float Perlin3D(float x, float y, float z) {
        float ab = Mathf.PerlinNoise(x, y);
        float bc = Mathf.PerlinNoise(y, z);
        float ac = Mathf.PerlinNoise(x, z);

        float ba = Mathf.PerlinNoise(y, x);
        float cb = Mathf.PerlinNoise(z, y);
        float ca = Mathf.PerlinNoise(z, x);

        float abc = ab + bc + ac + ba + cb + ca;
        return abc / 6f;
    }

    //all neighbour positions of a given voxelPosition
    private Vector3[] GetVertexNeighbours(Vector3 originalVertexPosition) {
        Vector3[] neighbours = new Vector3[] {
            originalVertexPosition - new Vector3(-1f, 0f, 0f),
            originalVertexPosition - new Vector3(1f, 0f, 0f),
            originalVertexPosition - new Vector3(0f, 1f, 0f),
            originalVertexPosition - new Vector3(0f, -1f, 0f),
            originalVertexPosition - new Vector3(0f, 0f, -1f),
            originalVertexPosition - new Vector3(0f, 0f, 1f)
        };

        return neighbours;
    }

    //return 0 if our map doesn't contain a value for that position
    public int ValueForPosition(Vector3 pos) {
        int value;
        if (values.TryGetValue(pos, out value)) {
            return value;
        } else {
            return 0;
        }
    }

}
