﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//simple Message object for our protocol
[System.Serializable]
public class Message {
    public string type;
    public string name;
    public string position;
    public string content;
    public string receiver;
    public string sender;
    public bool world;

    public Message() { }

    public Message(string type, string name, string position, string content,
                    string receiver, string sender, bool world) {
        this.type = type;
        this.name = name;
        this.position = position;
        this.content = content;
        this.receiver = receiver;
        this.sender = sender;
        this.world = world;
    }

    public string AsJson() {
        return JsonUtility.ToJson(this);
    }

    public Vector3Int ExtractPosition() {
        if(position == null || position.Length == 0) {
            Debug.Log("Message doesn't contain a position string!! returning (0,0,0)");
            return Vector3Int.zero;
        }
        string[] coordinates = position.Split(',');
        Vector3Int pos = new Vector3Int();
        pos.x = int.Parse(coordinates[0]);
        pos.y = int.Parse(coordinates[1]);
        pos.z = int.Parse(coordinates[2]);
        return pos;
    }

    override public string ToString() {
        return AsJson();
    }
    
}
