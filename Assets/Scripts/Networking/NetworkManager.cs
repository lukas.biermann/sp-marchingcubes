﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour {

    private TcpClient tcpClient;
    private Thread clientReceiveThread;
    public bool connected;

    private string networkName;
    private string ipAddress;
    private int port;

    [SerializeField]
    private Text chatText;

    public string newMessage;
    public bool receivedNewMessage;

    public bool readyToStartMultiplayer;
    public Vector3Int initialPosition = Vector3Int.zero;

    public static NetworkManager instance;

    //Use as Singleton
    private void Awake() {
        if (instance != null) {
            Destroy(instance);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    
    public void Connect(string name, string address, int port) {
        //create an extra thread, because socket blocks and it won't work with coroutines since unity even runs them on a single thread...
        networkName = name;
        ipAddress = address;
        this.port = port;
        clientReceiveThread = new Thread(new ThreadStart(ListenForData));
        clientReceiveThread.IsBackground = true;
        clientReceiveThread.Start();
    }

    /*
     * Networkcode from https://gist.github.com/danielbierwirth/0636650b005834204cb19ef5ae6ccedb
     */
    private void ListenForData() {
        try {
            tcpClient = new TcpClient(ipAddress, port);
        } catch (SocketException se) {
            Debug.Log("Error connecting to server: " + se);
            Thread.CurrentThread.Abort();
            return;
        }
        connected = true;
        Byte[] bytes = new Byte[1024];
        SendHelloMessageAfterConnecting();
        while (connected) {
            // Get a stream object for reading 				
            using (NetworkStream stream = tcpClient.GetStream()) {
                int length;
                // Read incomming stream into byte arrary. 					
                while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) {
                    var incommingData = new byte[length];
                    Array.Copy(bytes, 0, incommingData, 0, length);
                    // Convert byte array to string message. 						
                    string serverMessage = Encoding.ASCII.GetString(incommingData);
                    Debug.Log("server message received as: " + serverMessage);
                    Message message = MessageFactory.CreateFromJson(serverMessage);
                    ProcessMessage(message);
                }
            }
        }
    }

    //the reason why our processing stores everything in variables instead of executing methods by itself
    //is that unity functions can only be called from the main thread and our listener runs on a separate thread.
    //So what we do in every update is checking whether a new message was received via "receivedNewMessage" 
    //and then process the message in "newMessage" externally.
    private void ProcessMessage(Message message) {
        switch (message.type) {
            case "welcome":
                initialPosition = message.ExtractPosition();
                readyToStartMultiplayer = true;
                break;
            case "chat":
                Debug.Log("Received chat message, printing to textBox");
                DateTime date = DateTime.Now;
                string newChatMessage = "\n" 
                    + date.Hour + ":" + date.Minute 
                    + " " + message.sender;
                if (message.world) {
                    newChatMessage += " sagt: ";
                } else {
                    newChatMessage += " flüstert: ";
                }
                newChatMessage += message.content;
                newMessage = newChatMessage;
                receivedNewMessage = true;
                break;
            case "end":
                Debug.Log("Server shutting down, closing stream");
                Disconnect();
                break;
            default:
                break;
        }
    }

    private void SendHelloMessageAfterConnecting() {
        Message message = MessageFactory.CreateHelloMessage(networkName);
        SendNetworkMessage(message);
    }

    public void SendNetworkMessage(Message message) {
        if (!connected || tcpClient == null) {
            return;
        }
        // Get a stream object for writing. 			
        NetworkStream stream = tcpClient.GetStream();
        if (stream.CanWrite) {
            // Convert string message to byte array.                 
            byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(message.AsJson() + "\n");
            // Write byte array to socketConnection stream.                 
            Debug.Log("sending: " + message);
            stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
        }
    }

    public void Disconnect() {
        clientReceiveThread.Abort();
        connected = false;        
        tcpClient.Dispose();
    }
}
