﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//used to generate the default messages
public class MessageFactory {

    private MessageFactory() { }

    public static Message CreateHelloMessage(string name) {
        Message message = new Message();
        message.type = "hello";
        message.name = name;
        return message;
    }

    public static Message CreateChatMessage(string receiver, string content) {
        Message message = new Message();
        message.type = "chat";
        message.receiver = receiver;
        message.content = content;
        return message;
    }

    public static Message CreateFromJson(string json) {
        return JsonUtility.FromJson<Message>(json);
    }
}
