﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Voxel : MonoBehaviour {

    public enum Type {Dirt, Grass, Stone, Lava, Water, Cloud, Wood, Purple, Yellow, Rose, Black, Cyan};
    public Type type;
    private MeshRenderer meshRenderer;
    [SerializeField]
    private GameObject dispersalParticles = null;
    private Color dispersalColor;

    private void Awake() {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    //load values for material from the given VoxelType Selector
    public void LoadValues(BlockSelector selector) {
        type = selector.type;
        meshRenderer.material = selector.mat;
        meshRenderer.material.SetColor("_BaseColor", selector.color);
        dispersalColor = selector.dispersalColor;
    }

    public void DestroyVoxel() {
        //check if we should playy the particle effect:
        if (PlayerPrefs.GetInt("dispersal") == 1) {
            GameObject go = Instantiate(dispersalParticles, transform.position, Quaternion.identity);
            ParticleSystem pSystem = go.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule main = pSystem.main;
            main.startColor = dispersalColor;
            pSystem.Emit(500);
        }
        Destroy(gameObject);
    }

}
