﻿using SFB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//used to serialize to file/read from file by just writing coordinates + voxeltype into a .dat file using the standalone file browser from
// https://github.com/gkngkc/UnityStandaloneFileBrowser

public class Serializer : MonoBehaviour {

    private GameObject voxelPrefab;
    private Dictionary<Voxel.Type, BlockSelector> typeSelectorDictionary;

    public void Start() {
        voxelPrefab = Resources.Load<GameObject>("Prefabs/Voxel");
        typeSelectorDictionary = new Dictionary<Voxel.Type, BlockSelector>();
        GameObject[] selectors = Resources.LoadAll<GameObject>("Prefabs/Tiles");
        foreach(GameObject selectorObject in selectors) {
            BlockSelector selectorScript = selectorObject.GetComponent<BlockSelector>();
            typeSelectorDictionary.Add(selectorScript.type, selectorScript);
        }
    }

    public void SerializeToFile() {
        string location = StandaloneFileBrowser.SaveFilePanel("Save world", "", "world", "dat");
        if (location == null) {
            return;
        }
        StreamWriter writer = new StreamWriter(location, true);

        GameObject[] voxelObjects = GameObject.FindGameObjectsWithTag("Voxel");
        Vector3Int pos;
        foreach (GameObject voxelObject in voxelObjects) {
            Voxel voxel = voxelObject.GetComponent<Voxel>();
            pos = Vector3Int.RoundToInt(voxel.transform.position);
            writer.Write(pos.x + "," + pos.y + "," + pos.z + "," + (int)voxel.type + "\n");
        }
        writer.Flush();
        writer.Dispose();
        GameManager.instance.ToggleMenu();
    }

    public void DeserializeFromFile() {
        GameManager.instance.CleanUp();
        string[] locations = StandaloneFileBrowser.OpenFilePanel("Load world", "", "dat", false);
        if (locations == null) {
            return;
        }
        StreamReader reader = new StreamReader(locations[0]);
        Vector3Int pos;
        Voxel.Type type;
        Voxel createdVoxel;
        while (!reader.EndOfStream) {
            string[] data = reader.ReadLine().Split(',');
            if(data.Length < 4) {
                continue;
            }
            pos = new Vector3Int(int.Parse(data[0]), int.Parse(data[1]), int.Parse(data[2]));
            type = (Voxel.Type)int.Parse(data[3]);
            createdVoxel = Instantiate(voxelPrefab, pos, Quaternion.identity).GetComponent<Voxel>();
            createdVoxel.LoadValues(typeSelectorDictionary[type]);
            GameManager.instance.AddVoxel(pos, createdVoxel.gameObject);
        }
        GameManager.instance.ToggleMenu();
    }

    //just experimenting with pixel data from images.
    //goes through every pixel of the image and creates a voxel with the same color at that position
    public void GenerateFromImage() {
        ExtensionFilter filter = new ExtensionFilter("imageFilter", new string[] { "png", "jpg"});
        string[] locations = StandaloneFileBrowser.OpenFilePanel("Select image", "", new ExtensionFilter[] { filter }, false);
        if(locations == null) {
            return;
        }
        Texture2D tex = new Texture2D(0, 0);
        byte[] data = File.ReadAllBytes(locations[0]);
        tex.LoadImage(data);
        Color[] pixels = tex.GetPixels();
        int counter = 0;
        foreach(Color pixel in pixels) {
            if(pixel.a < 1f) {
                counter++;
                continue;
            }
            GameObject createdVoxel = Instantiate(voxelPrefab, new Vector3(counter % tex.width, counter / tex.width,0f), Quaternion.identity);
            createdVoxel.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", pixel);
            counter++;
        }
        GameManager.instance.ToggleMenu();
    }
    
}
