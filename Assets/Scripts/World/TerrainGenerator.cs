﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {

    [SerializeField]
    private GameObject voxelPrefab = null;

    [SerializeField]
    private int chunkSize = 50;

    [SerializeField]
    private float noiseScale = .05f;

    [SerializeField]
    private bool createSphere = false;

    [SerializeField, Range(0, 1)]
    private float threshold = .5f;

    [SerializeField]
    private BlockSelector defaultSelector = null;

    private void Generate() {
        //clean up before generating another random terrain
        GameManager.instance.CleanUp();
        
        //go through each block position
        for (int x = 0; x < chunkSize; x++) {
            for (int y = 0; y < chunkSize; y++) {
                for (int z = 0; z < chunkSize; z++) {

                    float noiseValue = Perlin3D(x * noiseScale, y * noiseScale, z * noiseScale);//get value of the noise at given x, y, and z.
                    if (noiseValue >= threshold) {//is noise value above the threshold for placing a block?
                        
                        Vector3Int pos = new Vector3Int(x, y, z);
                        //ignore this block if it's a sphere and it's outside of the radius (ex: in the corner of the chunk, outside of the sphere)
                        //distance between the current point with the center point. if it's larger than the radius, then it's not inside the sphere.
                        //same procedure as our placeholder/voxel placement with brushSize
                        if (createSphere) {
                            float raduis = chunkSize / 2;
                            if (Vector3.Distance(pos, Vector3.one * raduis) > raduis)
                                continue;
                        }

                        GameObject createdVoxel = Instantiate(voxelPrefab, pos, Quaternion.identity);
                        createdVoxel.GetComponent<Voxel>().LoadValues(defaultSelector);
                        GameManager.instance.AddVoxel(pos, createdVoxel);
                    }

                }
            }
        }
        
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.G)) {
            Generate();
        }
    }

    //3D perlin noise through unitys Mathf.PerlinNoise
    public static float Perlin3D(float x, float y, float z) {
        float ab = Mathf.PerlinNoise(x, y);
        float bc = Mathf.PerlinNoise(y, z);
        float ac = Mathf.PerlinNoise(x, z);

        float ba = Mathf.PerlinNoise(y, x);
        float cb = Mathf.PerlinNoise(z, y);
        float ca = Mathf.PerlinNoise(z, x);

        float abc = ab + bc + ac + ba + cb + ca;
        return abc / 6f;
    }

}