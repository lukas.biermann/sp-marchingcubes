﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    [SerializeField]
    private GameObject menuPanel = null;
    [SerializeField]
    private SelectionBar selectionBar = null;
    [SerializeField]
    private GameObject notificationContainer = null;
    [SerializeField]
    private GameObject playerPrefab = null;
    [SerializeField]
    private Cinemachine.CinemachineFreeLook cinemachineCamera = null;
    [SerializeField]
    private InputField chatInput = null;

    private Dictionary<Vector3Int, GameObject> voxels;

    private Vector3Int lowestBounds, highestBounds;

    private PlayerController playerController;
    private bool areVoxelsVisible = true;
    private bool typingChatMessage;

    private void Awake() {
        if(instance != null) {
            Destroy(instance);
        }
        if (!Application.isEditor) {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        //by default the dispersal effect should be played:
        if (!PlayerPrefs.HasKey("dispersal")) {
            PlayerPrefs.SetInt("dispersal", 1);
        }
        instance = this;
        lowestBounds = new Vector3Int(int.MaxValue, int.MaxValue, int.MaxValue);
        highestBounds = new Vector3Int(int.MinValue, int.MinValue, int.MinValue);
        voxels = new Dictionary<Vector3Int, GameObject>();
        //we can always retrieve the starting position from our Networkmanager since it also existst in singleplayer and has defaultvalues 0,0,0
        GameObject playerObject = Instantiate(playerPrefab, NetworkManager.instance.initialPosition, Quaternion.identity);
        playerController = playerObject.GetComponent<PlayerController>();
        cinemachineCamera.Follow = playerObject.transform;
        cinemachineCamera.LookAt = playerObject.transform;
    }

    //mainly used for marching cubes
    public void ToggleVoxelVisibility() {
        areVoxelsVisible = !areVoxelsVisible;
        foreach(GameObject go in voxels.Values) {
            go.SetActive(areVoxelsVisible);
        }
    }

    public void BackToMainMenu() {
        if (NetworkManager.instance.connected) {
            NetworkManager.instance.Disconnect();
        }
        SceneManager.LoadScene("MainMenu");
    }

    private void Update() {
        //Toggle dispersal effect:
        if (!typingChatMessage) {
            if (Input.GetKeyDown(KeyCode.T)) {
                ToggleDispersal();
            }
            if (Input.GetKeyDown(KeyCode.Escape)) {
                ToggleMenu();
            }
        }
        
        if (Input.GetKeyDown(KeyCode.Return) && NetworkManager.instance.connected) {
            HandleChat();            
        }
        RenderSettings.skybox.SetFloat("_Rotation", Time.time);
    }

    private void HandleChat() {
        if (!typingChatMessage) {
            typingChatMessage = true;
            //move focus to the chatInput so we see the carret and can start typing
            EventSystem.current.SetSelectedGameObject(chatInput.gameObject);
            playerController.EnableInputs = false;
        } else {
            typingChatMessage = false;
            playerController.EnableInputs = true;
            ProcessChatMessage(chatInput.text);
            chatInput.text = "";
        }        
    }

    //process chat message, whispering to a player is possible by appending '/w playername' to the message
    private void ProcessChatMessage(string message) {
        if(message.StartsWith("/w ")) {
            message = message.Remove(0, 3);//remove /w prefix
            int spaceIndex = message.IndexOf(" ");
            string receiver = message.Substring(0, spaceIndex);
            string content = message.Substring(spaceIndex + 1, message.Length - spaceIndex - 1);
            Debug.Log("whispering " + content + " to: " + receiver);
            NetworkManager.instance.SendNetworkMessage(MessageFactory.CreateChatMessage(receiver, content));
        } else {
            Debug.Log("Sending " + message + " to everyone");
            NetworkManager.instance.SendNetworkMessage(MessageFactory.CreateChatMessage("world", message));
        }
        
    }

    public void ToggleMenu() {
        menuPanel.SetActive(!menuPanel.activeSelf);
        //disable inputs when the menu is open and enable the cursor if it was disabled in a real Build application
        if (menuPanel.activeSelf) {
            playerController.EnableInputs = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        } else {
            if (!Application.isEditor) {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            playerController.EnableInputs = true;
        }
    }

    //toggle the dispersal effect when you destroy blocks
    private void ToggleDispersal() {
        string text = "Dispersal effect: ";
        if (PlayerPrefs.GetInt("dispersal") == 0) {
            text += "ON";
            PlayerPrefs.SetInt("dispersal", 1);
        } else {
            text += "OFF";
            PlayerPrefs.SetInt("dispersal", 0);
        }
        notificationContainer.GetComponentInChildren<TextMeshProUGUI>().text = text;
        notificationContainer.GetComponent<Animator>().Play("NotificationBlendInBlendOut");
    }

    public BlockSelector GetCurrentSelector() {
        return selectionBar.GetCurrentSelector();
    }

    //update lowest and highest bounds for marching cubes after each change in voxels
    public void AddVoxel(Vector3Int position, GameObject voxelObject) {
        voxels[position] = voxelObject;
        lowestBounds = Vector3Int.Min(lowestBounds, position);
        highestBounds = Vector3Int.Max(highestBounds, position);
    }

    //update lowest and highest bounds for marching cubes after each change in voxels
    public void RemoveVoxel(Vector3Int key) {
        voxels.Remove(key);
        //first check if we really have to recalculate the bounds which requires us to iterate through every cube
        //This should (obviously) be disabled or improved if marching cubes isn't our main interest anymore
        if(key == lowestBounds || key == highestBounds) {
            lowestBounds = Vector3Int.zero;
            highestBounds = Vector3Int.zero;
            foreach (Vector3Int pos in voxels.Keys) {
                lowestBounds = Vector3Int.Min(lowestBounds, pos);
                highestBounds = Vector3Int.Max(highestBounds, pos);
            }
        }
    }

    //used to cleanUp before loading a new world
    public void CleanUp() {
        foreach(GameObject go in voxels.Values) {
            Destroy(go);
        }
        voxels = new Dictionary<Vector3Int, GameObject>();
    }

    public Vector3Int GetLowestBounds() {
        return lowestBounds;
    }

    public Vector3Int GetHighestBounds() {
        return highestBounds;
    }

    public Dictionary<Vector3Int, GameObject> GetVoxels() {
        return voxels;
    }
}
