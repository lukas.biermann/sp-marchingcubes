﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBox : MonoBehaviour {
    [SerializeField]
    private GameObject chatArea = null;
    Text chatText;

    private void Awake() {
        chatText = GetComponent<Text>();
        if (!NetworkManager.instance.connected) {
            chatArea.SetActive(false);
        }
    }

    public void Update() {
        if (NetworkManager.instance.receivedNewMessage) {
            chatText.text += NetworkManager.instance.newMessage;
            NetworkManager.instance.receivedNewMessage = false;
        }
    }
}
