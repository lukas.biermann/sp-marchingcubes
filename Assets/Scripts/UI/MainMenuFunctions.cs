﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuFunctions : MonoBehaviour {

    [SerializeField]
    private InputField nameField = null;
    [SerializeField]
    private InputField addressField = null;
    [SerializeField]
    private InputField portField = null;

    public void ExitGame() {
        Application.Quit(0);
    }

    public void StartMultiplayer() {
        NetworkManager.instance.Connect(nameField.text, addressField.text, int.Parse(portField.text));
        StartCoroutine(WaitForConnection());
    }

    public void StartSingleplayer() {
        Debug.Log("Starting singleplayer");
        SceneManager.LoadScene("_Main");
    }

    private void ShowConnectionErrorMessage() {
        Debug.Log("Failed to connect to server!");
    }

    private void InitializeMultiplayer() {
        Debug.Log("Successfully connected to server: Starting multiplayer game");
        // the reason we are waiting for another condition is that we first need to receive the hello message from our server
        //so we can initialize the Player at the right position as soon as the game starts
        while(NetworkManager.instance.readyToStartMultiplayer == false) {

        }
        Debug.Log("Instantiating multiplayer with player at: " + NetworkManager.instance.initialPosition);
        SceneManager.LoadScene("_Main");
    }

    private IEnumerator WaitForConnection() {
        int count = 0;
        while(NetworkManager.instance.connected == false) {
            yield return new WaitForSeconds(0.1f);
            count++;
            if(count >= 30) {
                break;
            }
        }
        //if we broke after 3 seconds without connecting, show the error message, else init multiplayer
        if(NetworkManager.instance.connected == true) {
            InitializeMultiplayer();
            yield return null;
        }
        ShowConnectionErrorMessage();
        yield return null;
    }

}
