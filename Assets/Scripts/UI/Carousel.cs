﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//displays all blocks in our carousel. It's now not a carousel since all the blocks fit into one line of the UI
public class Carousel : MonoBehaviour
{
    private List<BlockSelector> selectors = new List<BlockSelector>();
    private int currentIndex;
    [SerializeField]
    private GameObject highlighter = null;
    [SerializeField]
    private TextMeshProUGUI textMeshProUGUI = null;
    private TileCategory category;


    public void SetCategory(TileCategory category) {
        this.category = category;
        foreach (GameObject selector in category.selectors) {
            AddTile(selector);
        }
        UpdateDescription();
        UpdateSize();
    }

    private void AddTile(GameObject tile) {
        GameObject go = Instantiate(tile, transform);
        selectors.Add(go.GetComponent<BlockSelector>());
    }

    private void UpdateDescription() {
        textMeshProUGUI.text = category.CategoryName;
    }

    private void UpdateSize() {
        float categoryWidth = category.GetComponent<RectTransform>().rect.width;
        //place with 6 pixels space between categories and borders:
        //the positioning is still a bit hardcoded (76f offset on the y axis) but this wasn't a priority for us
        GetComponent<RectTransform>().sizeDelta = new Vector2((categoryWidth + 6f) * selectors.Count + 6, 76f);
    }

    public BlockSelector SelectCurrentTile() {
        return selectors[currentIndex];
    }

    public BlockSelector SelectNextTile() {
        currentIndex = (currentIndex + 1) % selectors.Count;
        UpdateHighlighterPosition();
        return selectors[currentIndex];
    }

    public BlockSelector SelectPreviousTile() {
        currentIndex--;
        if(currentIndex == -1) {
            currentIndex = selectors.Count - 1;
        }
        UpdateHighlighterPosition();
        return selectors[currentIndex];
    }

    private void UpdateHighlighterPosition() {
        highlighter.transform.localPosition = selectors[currentIndex].transform.localPosition;
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }
}
