﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCategory : MonoBehaviour {

    public GameObject[] selectors;
    [SerializeField]
    private string categoryName = default;
    public string CategoryName { get => categoryName; }

}
