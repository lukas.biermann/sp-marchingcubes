﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/*
 * The Blockselection is responsible for the selection of the right blockSelector
 * It reacts to the playerinputs and moves the highlighter + UI
 */
public class SelectionBar : MonoBehaviour {

    [SerializeField]
    private GameObject carouselPrefab = null;

    //each category gets assigned a carousel
    private TileCategory[] categories;
    //each carousel contains all the BlockSelectors of the corresponding category
    private Carousel[] carousels;

    //which category is currently selected:
    private int currentCategory;
    //which actual Block is currently selected:
    private BlockSelector currentSelector;
    

    void Awake() {
        GameObject[] prefabs = Resources.LoadAll<GameObject>("Prefabs/Categories");

        //before adding the categories, adjust the width of the horizontal selectionbar:
        RectTransform rectTrans = GetComponent<RectTransform>();
        //get the width and height of our category images:
        float categoryWidth = prefabs[0].GetComponent<RectTransform>().rect.width;
        //place with 6 pixels space between categories and borders:
        rectTrans.sizeDelta = new Vector2((categoryWidth + 6f) * prefabs.Length + 6, 76f);

        categories = new TileCategory[prefabs.Length];
        carousels = new Carousel[prefabs.Length];
        GameObject go = null;
        for(int i = 0; i < prefabs.Length; i++) {
            go = Instantiate(prefabs[i], transform);
            categories[i] = go.GetComponent<TileCategory>();    //create Category objects

            BlockSelector firstSelector = categories[i].selectors[0].GetComponent<BlockSelector>();
            categories[i].GetComponent<Image>().sprite = firstSelector.GetComponent<Image>().sprite;
            categories[i].GetComponent<Image>().color = firstSelector.color;

            //Set hotkeys to the right numbers:
            //we do it here, because we know the order only at this point:
            categories[i].GetComponentInChildren<TextMeshProUGUI>().text = (i + 1).ToString();
            //calculate the position for the carousel. Since its anchors are in the center, we can just take the category position
            Vector3 carouselPosition = go.transform.position;

            //and add the category image height for spacing to the y component:
            carouselPosition.y += categoryWidth * transform.localScale.y + 6f;
            carousels[i] = Instantiate(carouselPrefab, carouselPosition, Quaternion.identity, go.transform) //attach carousel to the category
                                .GetComponent<Carousel>();
            //carousels are disabled by default, so no need to hide here

            //assign the right category, carousel does the rest:
            carousels[i].SetCategory(categories[i]);
        }

        //set the currentSelection to the very first tile:
        currentSelector = carousels[0].SelectCurrentTile();
    }

    private void Update() {
        for (int i = 1; i <= categories.Length; i++) {
            if (Input.GetKeyDown("" + i)) {
                OpenCategory(i - 1);    //check for input
            }
        }
        //a category was selected and the carousel is showing right now:
        if (currentCategory >= 0) {
            //check for input to change the selector:
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                currentSelector = carousels[currentCategory].SelectPreviousTile();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                currentSelector = carousels[currentCategory].SelectNextTile();
            }
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {
                CloseCategory();
            }
        }
    }

    //opens the category, showing all tiles in it:
    private void OpenCategory(int number) {
        if(currentCategory == number) {
            //we are already showing that category, so we assume the player wants to cycle through?
            currentSelector = carousels[currentCategory].SelectNextTile();
            return;
        }
        if(currentCategory >= 0 && currentCategory != number) {
            CloseCategory(); //close old category
            //but keep going to open the new one
        }
        
        currentCategory = number; //keep track of the category, so we can process playerInput accordingly
        Carousel openedCarousel = carousels[number];
        //We only give the carousel the first index of the selectors for an initial selection:
        openedCarousel.Show();
        currentSelector = openedCarousel.SelectCurrentTile();
    }

    private void CloseCategory() {
        carousels[currentCategory].Hide();
        currentCategory = -1;
    }

    //returns the current selector
    public BlockSelector GetCurrentSelector() {
        return currentSelector;
    }

}
