﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * A Blockselector keeps all neccessary information about a blocktype
 * Each Object in the UI has a BlockSelector, that should contain all needed information
 */
public class BlockSelector : MonoBehaviour {
    public Color color;
    public Voxel.Type type;
    public Material mat;
    public Color dispersalColor;

    private void Awake() {
        //assign the color to the UI image, in case we have no material yet so we can at least see the color
        GetComponent<Image>().color = color;
    }

}
