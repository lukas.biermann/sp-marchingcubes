﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayerMovement : MonoBehaviour
{

    private float randomOffset;

    //Let the player float around and dissolve from time to time while in the main menu
    void Update() {
        if(randomOffset <= 0) {
            StartCoroutine(DissolveRoutine());
            randomOffset = Random.Range(5f, 15f);
        }
        randomOffset -= 1f * Time.deltaTime;
        transform.Translate(transform.right * Time.deltaTime);
        transform.RotateAround(Vector3.zero, Vector3.up, 30f * Time.deltaTime);
    }

    private IEnumerator DissolveRoutine() {
        Material mat = GetComponent<MeshRenderer>().material;
        for (int i = 0; i <= 50; i++) {
            mat.SetFloat("_DissolveGrade", i * 2 / 100f);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        yield return new WaitForSeconds(0.2f);
        for (int i = 50; i >= 0; i--) {
            mat.SetFloat("_DissolveGrade", i * 2 / 100f);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        yield return null;
    }
}
