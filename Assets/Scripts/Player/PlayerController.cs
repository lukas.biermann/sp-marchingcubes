﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/**
 * Processes Playerinputs for moving and placing voxels.
 * Has all methods for creating/altering/deleting voxels
 * And offers methods for teleporting/fast movement 
 */
public class PlayerController : MonoBehaviour {

    [SerializeField]
    [Range(0.1f, 10f)]
    private float maxSpeed = 10f;

    [SerializeField]
    [Range(1.5f, 5f)]
    private float sprintSpeedMultiplier = 2f;

    [SerializeField]
    private float defaultPlaceDistance = 12f;
    [SerializeField]
    private float minPlaceDistance = 9f;
    [SerializeField]
    private float maxPlaceDistance = 40f;

    [SerializeField]
    private LayerMask voxelDetectionLayer = default;

    private float horizontalInput, verticalInput;
    private float brushSize = 0.5f;
    private bool brushChanged;

    //will all be set during runtime:
    private GameObject voxelPrefab;
    private GameObject voxelPlaceholderPrefab;
    private GameObject voxelPlaceholderInstance;
    private MeshRenderer voxelPlaceholderRenderer;
    private List<GameObject> additionalPlaceholders;
    private CinemachineFreeLook freeLookCamera;

    private bool enableInputs = true;
    public bool EnableInputs {
        get { return enableInputs; }
        set {
            enableInputs = value;
            freeLookCamera.enabled = value;
        }
    }

    //disable mouse cursor and lock it, so it's easier to look around
    private void OnApplicationFocus(bool focus) {
        if (focus && !Application.isEditor) {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    //load prefabs from ressources and instantiate placeholder
    void Start() {
        voxelPrefab = Resources.Load<GameObject>("Prefabs/Voxel");
        voxelPlaceholderPrefab = Resources.Load<GameObject>("Prefabs/VoxelPlaceholder");
        freeLookCamera = GameObject.Find("ThirdPersonCamera").GetComponent<CinemachineFreeLook>();

        voxelPlaceholderInstance = InstantiateAtPosition(voxelPlaceholderPrefab, GetVoxelPlacementPosition(), false);
        voxelPlaceholderRenderer = voxelPlaceholderInstance.GetComponent<MeshRenderer>();
        additionalPlaceholders = new List<GameObject>();
    }

    // Update is called once per frame
    void Update() {
        if (EnableInputs) {
            ProcessInputs();
        }
        Move();
        UpdatePlaceholder();
    }

    private void ProcessInputs() {
        //get movement from axis:
        horizontalInput = Input.GetAxis("Horizontal") * maxSpeed;
        verticalInput = Input.GetAxis("Vertical") * maxSpeed;
        if (Input.GetAxisRaw("Sprint") > 0) {
            horizontalInput *= sprintSpeedMultiplier;
            verticalInput *= sprintSpeedMultiplier;
        }
        //check for hotkeys
        if (Input.GetMouseButtonDown(0)) {
            PlaceBlock();
        } else if (Input.GetMouseButtonDown(1)) {
            DestroyBlock();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            SpeedTravel();
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            Teleport();
        }
        //bruchsize:
        if(Input.GetKeyDown(KeyCode.Period)) {
            brushSize = Mathf.Min(5.5f, brushSize + 0.5f);
            Debug.Log("Brushsize: " + brushSize);
            brushChanged = true;
        }
        if (Input.GetKeyDown(KeyCode.Comma)) {
            brushSize = Mathf.Max(0.5f, brushSize - 0.5f);
            Debug.Log("Brushsize: " + brushSize);
            brushChanged = true;
        }
        //placement distance:
        float scroll = Input.GetAxisRaw("Mouse ScrollWheel");
        if(scroll < 0) {
            defaultPlaceDistance = Mathf.Max(defaultPlaceDistance - 1, minPlaceDistance);            
        } else if (scroll > 0) {
            defaultPlaceDistance = Mathf.Min(defaultPlaceDistance + 1, maxPlaceDistance);
        }
    }

    private void DestroyBlock() {
        Vector3 centerOfScreen = new Vector3(Camera.main.pixelWidth / 2f, Camera.main.pixelHeight / 2f, defaultPlaceDistance);
        Ray ray = Camera.main.ScreenPointToRay(centerOfScreen);
        RaycastHit hit; //to store information

        //actually cast the ray, store info into hit, distance is defaultPlaceDistance, only check for Voxel layer
        Physics.Raycast(ray, out hit, Mathf.Infinity, voxelDetectionLayer);
        if (hit.collider != null) {
            GameObject hitObject = hit.collider.gameObject;
            hitObject.GetComponent<Voxel>().DestroyVoxel();
            //remove voxel via the GameManagers method:
            GameManager.instance.RemoveVoxel(Vector3Int.RoundToInt(hitObject.transform.position));
        }
    }

    private void SpeedTravel() {
        Vector3 centerOfScreen = new Vector3(Camera.main.pixelWidth / 2f, Camera.main.pixelHeight / 2f, defaultPlaceDistance);
        Ray ray = Camera.main.ScreenPointToRay(centerOfScreen);
        RaycastHit hit; //to store information

        //actually cast the ray, store info into hit, distance is defaultPlaceDistance, only check for Voxel layer
        Physics.Raycast(ray, out hit, Mathf.Infinity, voxelDetectionLayer);
        if (hit.collider != null) {
            //we hit a voxel -> start Coroutine
            Vector3 hitpoints = hit.point;
            Vector3 freePosition = hit.point - (hit.point - transform.position).normalized * 1.5f;
            StartCoroutine(SpeedTravelRoutine(freePosition));
        }
    }

    private IEnumerator SpeedTravelRoutine(Vector3 targetPosition) {
        float sqrDistance = (targetPosition - transform.position).sqrMagnitude;
        int steps = 50;
        if (sqrDistance < 9) {
            steps = 5;
        }
        else if (sqrDistance < 50) {
            steps = 10;
        }
        else if (sqrDistance < 200) {
            steps = 20;
        }
        Vector3 travelVector = (targetPosition - transform.position) / steps;
        for (int i = 0; i < steps; i++) {
            transform.position += travelVector;
            yield return new WaitForSecondsRealtime(0.01f);
        }
        yield return null;
    }

    private void Teleport() {
        Vector3 centerOfScreen = new Vector3(Camera.main.pixelWidth / 2f, Camera.main.pixelHeight / 2f, defaultPlaceDistance);
        Ray ray = Camera.main.ScreenPointToRay(centerOfScreen);
        RaycastHit hit; //to store information

        //actually cast the ray, store info into hit, distance is defaultPlaceDistance, only check for Voxel layer
        Physics.Raycast(ray, out hit, Mathf.Infinity, voxelDetectionLayer);
        if (hit.collider != null) {
            //we can teleport to a voxel
            Vector3 hitpoints = hit.point;
            Vector3 freePosition = hit.point - (hit.point - transform.position).normalized * 1.5f;
            StartCoroutine(TeleportRoutine(freePosition));
        }
    }

    private IEnumerator TeleportRoutine(Vector3 targetPosition) {
        //get the material and change the DissolveGrade of our shader
        Material mat = GetComponent<MeshRenderer>().material;
        for(int i = 0; i <= 25; i++) {
            mat.SetFloat("_DissolveGrade", i * 4 / 100f);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        transform.position = targetPosition;
        yield return new WaitForSeconds(0.1f);
        for (int i = 25; i >= 0; i--) {
            mat.SetFloat("_DissolveGrade", i * 4 / 100f);
            yield return new WaitForSecondsRealtime(0.01f);
        }
        yield return null;
    }

    private void Move() {
        Vector3 forward = Camera.main.transform.forward;
        Vector3 right = Camera.main.transform.right;
        forward.Normalize();
        right.Normalize();
        Vector3 velocity = forward * verticalInput + right * horizontalInput;
        //basic inputs.. maybe using Translate wasn't the best choice regarding collision checking but that isn't a priority for us
        transform.Translate(velocity * Time.deltaTime);
    }

    //destroy old placeholders and check for every position inside a cube of dimension "brushSize" whether it's within the radius "brushSize"
    //if it is, add that position to the placeholder List
    private void ChangeAdditionalPlaceholders() {
        Vector3Int placementPosition = Vector3Int.RoundToInt(voxelPlaceholderInstance.transform.position);
        foreach (GameObject placeholder in additionalPlaceholders) {
            //there's probably a better solution (reusing) than deleting every old placeholder.
            Destroy(placeholder);
        }
        if (brushSize > 0.5f) {            
            additionalPlaceholders = new List<GameObject>();
            int intSize = (int)brushSize;
            for (int x = placementPosition.x - intSize; x <= placementPosition.x + intSize; x++) {
                for (int y = placementPosition.y - intSize; y <= placementPosition.y + intSize; y++) {
                    for (int z = placementPosition.z - intSize; z <= placementPosition.z + intSize; z++) {
                        Vector3Int pos = new Vector3Int(x, y, z);
                        if (Vector3.Distance(placementPosition, pos) <= brushSize) {
                            additionalPlaceholders.Add(InstantiateAtPosition(voxelPlaceholderPrefab, pos, false));
                        }
                    }
                }
            }
        }
        
       
    }

    //updatePlacholder checks whether the bushSize has changed. If so, delete old placeholders and recreate accordingly
    private void UpdatePlaceholder() {
        //get the currently selected block
        BlockSelector selector = GameManager.instance.GetCurrentSelector();
        //Set values for the placeholder manually, since we have to adjust the alpha:
        Color placeholderColor = selector.color;
        placeholderColor.a = 0.25f;
        voxelPlaceholderRenderer.material.SetColor("_BaseColor", placeholderColor);
        //only copy texture not the whole material, since we use a transparent one for the placeholder:
        voxelPlaceholderRenderer.material.SetTexture("_BaseMap", selector.mat.GetTexture("_BaseMap"));
        float dist = Vector3.Distance(voxelPlaceholderInstance.transform.position, GetVoxelPlacementPosition());
        //only really change something if the Placeholder has moved away at least 1 unit from the VoxelPlacementPosition (so it's now in another grid)
        //the reason for this is that for a big brushSize - a sphere with 40+ Placeholders checking the movement every frame and then only snapping to the same gridposition again is not necessary
        if (dist >= 1 && !brushChanged) {
            //only change the position if the brush hasn't changed, else we can just Recreate accordingly and at the right position
            Vector3 diff = GetVoxelPlacementPosition() - voxelPlaceholderInstance.transform.position;
            voxelPlaceholderInstance.transform.position += diff;
            foreach (GameObject placeholder in additionalPlaceholders) {
                if(placeholder != null) {
                    placeholder.transform.position += diff;
                }                
            }
        }
        if (brushChanged) {
            ChangeAdditionalPlaceholders();
            brushChanged = false;
        }
    }

    //Instantiate at the current PlacementPosition.
    //if brushSize is bigger than one block, instantiate for every position that's inside the brushSize Sphere
    private void PlaceBlock() {
        Vector3Int placementPosition = GetVoxelPlacementPosition();
        
        if(brushSize > 0.5f) {
            int intSize = (int)brushSize;
            for(int x = placementPosition.x - intSize; x <= placementPosition.x + intSize; x++) {
                for(int y = placementPosition.y - intSize; y <= placementPosition.y + intSize; y++) {
                    for(int z = placementPosition.z - intSize; z <= placementPosition.z + intSize; z++) {
                        Vector3 pos = new Vector3(x, y, z);
                        if(Vector3.Distance(placementPosition, pos) <= brushSize) {
                            //check if there already is a voxel and only place if the position is empty
                            if (Physics.OverlapSphere(pos, 0.25f, voxelDetectionLayer).Length > 0) {
                                continue;
                            }
                            InstantiateAtPosition(voxelPrefab, new Vector3Int(x, y, z));
                        }                        
                    }
                }
            }
        } else {
            InstantiateAtPosition(voxelPrefab, placementPosition);
        }
        
    }

    private Vector3Int GetVoxelPlacementPosition() {
        //Get screen point of targetPlcementPosition -> middle of screen with specified z distance as our defaultPlacementDistance:
        Vector3 centerOfScreen = new Vector3(Camera.main.pixelWidth / 2f, Camera.main.pixelHeight / 2f, defaultPlaceDistance);
        
        //Raycast from Camera point into world, to check if space is empty:
        Ray ray = Camera.main.ScreenPointToRay(centerOfScreen);
        RaycastHit hit; //to store information

        //actually cast the ray, store info into hit, distance is defaultPlaceDistance, only check for Voxel layer
        Physics.Raycast(ray, out hit, defaultPlaceDistance, voxelDetectionLayer);
        if (hit.collider != null) {
            //space is already occupied
            Debug.DrawLine(transform.position, hit.point, Color.red);

            //get the offsetPosition snapped to our Grid where we can place the other voxel:
            Vector3Int newPosition = CalculateOffsetPosition(Vector3Int.RoundToInt(hit.collider.transform.position), hit.point);
            //draw a line so we can see the corrected placement position in editor
            Debug.DrawLine(transform.position, newPosition, Color.green);
            return newPosition;
        } else {
            //hooray space is empty, get default world position of our desired placement:
            Vector3 placementInWorld = Camera.main.ScreenToWorldPoint(centerOfScreen);

            //RoundToInt, to snap to our Grid:
            return Vector3Int.RoundToInt(placementInWorld);
        }        
    }

    private Vector3Int CalculateOffsetPosition(Vector3Int objectCenter, Vector3 hitPoint) {
        Vector3 offsetFromCenter = objectCenter - hitPoint;

        //calculate the offset from the center of the voxel that we hit
        //Since the hit point is exactly on one surface of the cube,
        //the offset vectors will have only onxe maximum or minimum value of 0.5
        //they will always look something like this: (0.2, -0.17, 0.5)

        //what we need to do is to only keep the 0.5 values and their sign
        //multiply by 2, so every 0.5 becomes 1 and -0.5 becomes -1
        offsetFromCenter *= 2;
        //cast to integer instead of Vector3Int.RoundToInt to cut unwanted values to 0
        Vector3Int normalizedOffset = new Vector3Int((int)offsetFromCenter.x, (int)offsetFromCenter.y, (int)offsetFromCenter.z);

        //because we calculated the offset by subtracting the hitpoint from the center
        //we now also need to substract the normalizedOffset from the center to offset to the right direction
        return objectCenter - normalizedOffset;
    }

    //Instantiate the given Prefab at the next best empty placementPosition and return the created gameobject
    private GameObject InstantiateAtPosition(GameObject prefab, Vector3Int placementPosition, bool isRealBlock = true) {
        GameObject go = Instantiate(prefab, placementPosition, Quaternion.identity);
        if (isRealBlock) { //only automatically load values for real Blocks, so the plceholders can manage them on their own
            Voxel v = go.GetComponent<Voxel>();
            BlockSelector selector = GameManager.instance.GetCurrentSelector();
            v.LoadValues(selector);
            GameManager.instance.AddVoxel(placementPosition, go);
        }
        return go;
    }
}
