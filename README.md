# SP-MarchingCubes

Marching Cubes implementation for the lecture Spieleprogrammierung

This repo contains the whole project and a single build for windows.
When starting from the editor it's important to start with the MainMenu Scene.
The project also contains three example wordls which can be loaded via the Menu.

The marching cubes algorithm is implemented in the three classes in scripts/marching cubes.
**AlgorithmSetup** calculates all necessary values for the algorithm.
**MarchingCubesMain** is the entry point and manages all cubes/meshes.
**MarchingCubes** implements the actual algorithm for a single cube.

Since the game has a lot of hotkeys but no indication in the game, here is a full
list of all hotkeys and their functionality:

# Hotkeys
General:
*  ESC: open/close menu
*  Enter: show/hide cubes

Movement:
*  WASD: movement
*  Shift: hold for sprint
*  E: speed travel to the targeted cube
*  Q: teleport to the targeted cube

Placing/removing blocks:  
*  Left mouse: create target cube
*  Right mouse: remove target cube
*  Scroll wheel: adjust placement distance
*  1-5: cycle through textures (right/left click to close selection)
*  T: toggle voxel destroy effect
*  G: generate random terrain (parameters can only be changed via Editor)
*  ./,: increase/decrease brush size

MarchingCubes: 
*  M: perform marching cubes on all voxels with the currently selected texture
*  N: perform marching cubes on a random 100x100x100 block
*  B: perform marching cubes on a random 200x200x200 block
*  ArrowUp/ArrowDown: inncrease/decrease Iso-Level for marching cubes (recreates meshes immediately)